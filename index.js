var incompleteTasksHolder = document.getElementById("myUL");
var completedTasksHolder = document.getElementById("completed-tasks");
var checkBox;
var myNodelist = document.getElementsByTagName("LI");
var children = document.getElementsByTagName("LI");
var i;
for (i = 0; i < myNodelist.length; i++) {
  var span = document.createElement("SPAN");
  var txt = document.createTextNode("\u00D7");
  span.className = "close";
  span.appendChild(txt);
  myNodelist[i].appendChild(span);
}


var close = document.getElementsByClassName("close");
var i;
for (i = 0; i < close.length; i++) {
  close[i].onclick = function() {
    var div = this.parentElement;
    div.style.display = "none";
  }
}

function newElement() {

  var li = document.createElement("li");
  var inputValue = document.getElementById("myInput").value;
  //   window.localStorage.setItem("key", inputValue)
  // let testVar = window.localStorage.getItem("key")
  //   debugger;

  var t = document.createTextNode(inputValue);
  li.appendChild(t);
  var b=/.*\S.*/;
  if ((inputValue === '')){
    document.getElementById("war").innerHTML = "You must write something!";
  } else {
    document.getElementById("myUL").appendChild(li);
    document.getElementById("war").innerHTML = "Task added";
  }
  document.getElementById("myInput").value = "";

  var span = document.createElement("SPAN");
  var txt = document.createTextNode("\u00D7");
   var checkBox=document.createElement("input");
  checkBox.type="checkbox";
  checkBox.id="chk";
  span.className = "close";
  span.appendChild(txt);
  li.appendChild(checkBox);
  li.appendChild(span);
  
  for (i = 0; i < close.length; i++) {
    close[i].onclick = function() {
      var div = this.parentElement;
      div.style.display = "none";
    }
  }
}

// Mark a task as complete
var taskCompleted = function() {
    console.log("task complete");
    // append the task list item to the completed-tasks
    var li = this.parentNode;
    completedTasksHolder.appendChild(li);
    bindTaskEvents(li, taskIncomplete);
}

// Mark a task as incomplete
var taskIncomplete = function() {
    console.log("task incomplete");
    // append to the incomplete-tasks
    var li = this.parentNode;
    incompleteTasksHolder.appendChild(li);
    bindTaskEvents(li, taskCompleted);
}

var bindTaskEvents = function(taskListItem, checkBoxEventHandler) {
    console.log("bind events");
    // select it's children
    var checkBox = taskListItem.querySelector("input[type=checkbox]");
  
    // bind checkBoxEventHandler to checkbox
    checkBox.onchange = checkBoxEventHandler;

}

// cycle over incompleteTaskHolder ul list items
for (var i = 0; i < incompleteTasksHolder.children.length; i++) {
    // bind events to list item's children (taskCompleted)
    bindTaskEvents(incompleteTasksHolder.children[i], taskCompleted);
}

//cycle over completeTaskHolder ul list items
for (var i = 0; i < completedTasksHolder.children.length; i++) {
    // bind events to list item's children (taskCompleted)
    bindTaskEvents(completedTasksHolder.children[i], taskIncomplete);
}