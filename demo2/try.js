var taskInput=document.getElementById("new-task");
var war=document.getElementById("war");
var addButton=document.getElementsByTagName("button")[0];
var incompleteTaskHolder=document.getElementById("incomplete-tasks");
var completedTasksHolder=document.getElementById("completed-tasks");

var createNewTaskElement=function(taskString){

    var listItem=document.createElement("li");
    var checkBox=document.createElement("input");  
    var label=document.createElement("label");
    var editInput=document.createElement("input");
    var editButton=document.createElement("button");
    var deleteButton=document.createElement("button");

    label.innerText=taskString;
    checkBox.type="checkbox";
    editInput.type="text";
    editInput.className="text";
    editButton.innerText="Edit";
    editButton.className="edit";
    deleteButton.innerText="X";
    deleteButton.className="delete";
    listItem.appendChild(checkBox);
    listItem.appendChild(label);
    listItem.appendChild(editInput);
    listItem.appendChild(editButton);
    listItem.appendChild(deleteButton);
    return listItem;
}

function first(){
    var getTasks = window.localStorage.getItem('arrayList');
    if(getTasks == null || getTasks == undefined || getTasks === '') {
        window.localStorage.setItem('arrayList', JSON.stringify([]));
    }
    var retainTasks = JSON.parse(window.localStorage.getItem('arrayList'));
    for (var i = 0; i < retainTasks.length; i++) {
        var input=retainTasks[i].task;
        var input1=retainTasks[i].status;
        if(input1=='incomplete'){
          var listItem=createNewTaskElement(input);
          incompleteTaskHolder.appendChild(listItem);
          bindTaskEvents(listItem, taskCompleted);}
        else{
            var listItem=createNewTaskElement(input);
            completedTasksHolder.appendChild(listItem);
            bindTaskEvents(listItem, taskIncomplete);
        }
     }
  }
  
var addTask=function(){
    var listItem=createNewTaskElement(taskInput.value);
    if(taskInput.value.match(/^\s*\s*$/) ) 
          {
             alert("Enter something valid..!");
             false;
          }
    else{

            var retainTasks = JSON.parse(window.localStorage.getItem('arrayList'));
            retainTasks.push({task: taskInput.value, status: 'incomplete'})
            window.localStorage.setItem('arrayList', JSON.stringify(retainTasks));
            let getTask = JSON.parse(window.localStorage.getItem('arrayList'));
            var listItem=createNewTaskElement(taskInput.value);
            incompleteTaskHolder.appendChild(listItem);
            bindTaskEvents(listItem, taskCompleted);
            taskInput.value=" ";
            document.getElementById("war").innerHTML = "Added";
    }
}
var editTask=function(){
    var listItem=this.parentNode;
    var editInput=listItem.querySelector('input[type=text]');
    var label=listItem.querySelector("label");
    var containsClass=listItem.classList.contains("editMode");
    var taskToEdit = listItem.childNodes[1].textContent;
    var retainTasks = JSON.parse(window.localStorage.getItem('arrayList'));
    if(containsClass){
        label.innerText=editInput.value;
        var a=editInput.value;
        var len=retainTasks.length;
        let index = retainTasks.findIndex(todo => todo.task === taskToEdit);
        retainTasks[index].task = a;
        window.localStorage.setItem('arrayList', JSON.stringify(retainTasks));
        let getTask = JSON.parse(window.localStorage.getItem('arrayList'));
    }
    else{
        editInput.value=label.innerText;
    b = listItem.childNodes[1].textContent
    }
    
    listItem.classList.toggle("editMode");
}

var deleteTask=function(){

        var listItem=this.parentNode;
        var ul=listItem.parentNode;       
        var retainTasks = JSON.parse(window.localStorage.getItem('arrayList'));
        let value = listItem.innerText.split('\n')[0];
        var index = retainTasks.findIndex(x => x.task==value);
        retainTasks.splice(index,1);
        window.localStorage.setItem('arrayList', JSON.stringify(retainTasks));
        ul.removeChild(listItem);

}

var taskCompleted=function(){

        var listItem=this.parentNode;
        var retainTasks = JSON.parse(window.localStorage.getItem('arrayList'));
        var value = listItem.childNodes[1].textContent
        var len=retainTasks.length;
        for (var i = 0; i < len; i++) {
          var a=retainTasks[i].task
            if(a==value)
              {retainTasks[i].status='complete';
              }
           }
        window.localStorage.setItem('arrayList', JSON.stringify(retainTasks));
        let getTask = JSON.parse(window.localStorage.getItem('arrayList'));
        completedTasksHolder.appendChild(listItem);
        bindTaskEvents(listItem, taskIncomplete);
}
var taskIncomplete=function(){
        var listItem=this.parentNode;
        incompleteTaskHolder.appendChild(listItem);
        bindTaskEvents(listItem,taskCompleted);
        var retainTasks = JSON.parse(window.localStorage.getItem('arrayList'));
        let value = listItem.childNodes[1].textContent
        retainTasks.forEach((one)=>
            {if(one.task=== value)
            {
                one.status='incomplete';
            }
          }) ; 
        window.localStorage.setItem('arrayList', JSON.stringify(retainTasks));
        let getTask = JSON.parse(window.localStorage.getItem('arrayList'));
}

addButton.addEventListener("click",addTask);
var bindTaskEvents=function(taskListItem,checkBoxEventHandler){

    var checkBox=taskListItem.querySelector("input[type=checkbox]");
    var deleteButton=taskListItem.querySelector("button.delete");
    var editButton=taskListItem.querySelector("button.edit");
    editButton.onclick=editTask;
    deleteButton.onclick=deleteTask;
    checkBox.onchange=checkBoxEventHandler;
}

    for (var i=0; i<incompleteTaskHolder.children.length;i++){
     bindTaskEvents(incompleteTaskHolder.children[i],taskCompleted);
    }
    for (var i=0; i<completedTasksHolder.children.length;i++){
         bindTaskEvents(completedTasksHolder.children[i],taskIncomplete);
    }
