unction first(){
    localStorage.clear();
    var getTasks = window.localStorage.getItem('arrayList');
    if(getTasks == null || getTasks == undefined || getTasks === '') {
      window.localStorage.setItem('arrayList', JSON.stringify([]));
    }
    var retainTasks = JSON.parse(window.localStorage.getItem('arrayList'));
    console.log('onload task list is: ', retainTasks);
    

  }
var addTask=function(){
    var listItem=createNewTaskElement(taskInput.value);
    if(taskInput.value.match(/^\s*\s*$/) ) 
          {
             alert("Enter something valid..!");
             false;
          }
    else{

              var retainTasks = JSON.parse(window.localStorage.getItem('arrayList'));
              retainTasks.push({task: taskInput.value, status: 'incomplete'})
              window.localStorage.setItem('arrayList', JSON.stringify(retainTasks));
              let getTask = JSON.parse(window.localStorage.getItem('arrayList'));
              var listItem=createNewTaskElement(taskInput.value);
              incompleteTaskHolder.appendChild(listItem);
              bindTaskEvents(listItem, taskCompleted);
              taskInput.value=" ";
              document.getElementById("war").innerHTML = "Added";
    }
}
var editTask=function(){
console.log("Edit Task...");
console.log("Change 'edit' to 'save'");


var listItem=this.parentNode;

var editInput=listItem.querySelector('input[type=text]');
var label=listItem.querySelector("label");
var containsClass=listItem.classList.contains("editMode");
        if(containsClass){

            label.innerText=editInput.value;
        }else{
            editInput.value=label.innerText;
        }

        listItem.classList.toggle("editMode");
}

var deleteTask=function(){

        var listItem=this.parentNode;
        var ul=listItem.parentNode;       
        
        var retainTasks = JSON.parse(window.localStorage.getItem('arrayList'));
        let value = listItem.innerText.split('\n')[0];
        var removeByAttr = function(arr, attr, value1){
             var i = arr.length;
            while(i--){
             if( arr[i] 
             && arr[i].hasOwnProperty(attr) 
             && (arguments.length > 2 && arr[i][attr] === value1 ) ){ 
debugger;
           arr.splice(i,1);

          }
         }
         return arr;
        }
        removeByAttr(retainTasks, 'task', value);   

        ul.removeChild(listItem);

}
var taskCompleted=function(){

      var listItem=this.parentNode;
        completedTasksHolder.appendChild(listItem);
        bindTaskEvents(listItem, taskIncomplete);
        var retainTasks = JSON.parse(window.localStorage.getItem('arrayList'));
        let value = listItem.innerText.split('\n')[0];
        retainTasks.push({task: value, status: 'complete'})
        window.localStorage.setItem('arrayList', JSON.stringify(retainTasks));
        let getTask = JSON.parse(window.localStorage.getItem('arrayList'));

}
var taskIncomplete=function(){
        var listItem=this.parentNode;
        incompleteTaskHolder.appendChild(listItem);
        bindTaskEvents(listItem,taskCompleted);
}

addButton.addEventListener("click",addTask);

var bindTaskEvents=function(taskListItem,checkBoxEventHandler){

    var checkBox=taskListItem.querySelector("input[type=checkbox]");
    var deleteButton=taskListItem.querySelector("button.delete");
    var editButton=taskListItem.querySelector("button.edit");

            editButton.onclick=editTask;
            deleteButton.onclick=deleteTask;
            checkBox.onchange=checkBoxEventHandler;
}

    for (var i=0; i<incompleteTaskHolder.children.length;i++){
     bindTaskEvents(incompleteTaskHolder.children[i],taskCompleted);
    }
    for (var i=0; i<completedTasksHolder.children.length;i++){
         bindTaskEvents(completedTasksHolder.children[i],taskIncomplete);
    }


// function first(){
//     var getTasks = window.localStorage.getItem('arrayList');
//     if(getTasks == null || getTasks == undefined || getTasks === '') {
//       window.localStorage.setItem('arrayList', JSON.stringify([]));
//     }
//     var retainTasks = JSON.parse(window.localStorage.getItem('arrayList'));
//     console.log('onload task list is: ', retainTasks);
//   }

// var addTask=function(){
      
//        if(taskInput.value.match(/^\s*\s*$/) ) 
//           {
//              alert("Enter something valid..!");
//              false;
//           }
//         else{
//               //alert("Add task?");
//               console.log("Add Task...");
//               var retainTasks = JSON.parse(window.localStorage.getItem('arrayList'));
//               retainTasks.push({task: taskInput.value, status: 'incomplete'})
//               window.localStorage.setItem('arrayList', JSON.stringify(retainTasks));
//               let getTask = JSON.parse(window.localStorage.getItem('arrayList'));
//               //debugger;
//               var listItem=createNewTaskElement(taskInput.value);


//               incompleteTaskHolder.appendChild(listItem);
//               bindTaskEvents(listItem, taskCompleted);
//               taskInput.value=" ";
//           }
// }
// var listItem=this.parentNode;
//         completedTasksHolder.appendChild(listItem);
//         bindTaskEvents(listItem, taskIncomplete);
//         var retainTasks = JSON.parse(window.localStorage.getItem('arrayList'));
//         let value = listItem.innerText.split('\n')[0];
//         //debugger;
//         retainTasks.push({task: value, status: 'complete'})
//         window.localStorage.setItem('arrayList', JSON.stringify(retainTasks));
//         let getTask = JSON.parse(window.localStorage.getItem('arrayList'));