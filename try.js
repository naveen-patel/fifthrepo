var taskInput = document.getElementById("new-task");
var addButton = document.getElementById("addButton");
var incompleteTasksHolder = document.getElementById("incomplete-tasks");
var completedTasksHolder = document.getElementById("completed-tasks");

// New task list item
var createNewTaskElement = function(taskString) {
    // Create list item
    var listItem = document.createElement("li");

    // input (checkbox)
    var checkBox = document.createElement("input"); // checkbox

    // label
    var label = document.createElement("label");

    // input (text)
    var editInput = document.createElement("input"); // edit input text

    // button.edit
    var editButton = document.createElement("button") // button.edit

    // button.delete
    var deleteButton = document.createElement("button"); // button.delete

    // each elements need to be modified 


    // each element needs appended
    listItem.appendChild(checkBox);
    listItem.appendChild(label);
    listItem.appendChild(editInput);
    listItem.appendChild(editButton);
    listItem.appendChild(deleteButton);

    return listItem;
}

// Add a new task
var addTask = function() {
    console.log("add task");
    // Create a new list item with the text from #new-task:
    var listItem = createNewTaskElement("Some new task");

    // Append listItem to incompleteTaskHolder
    incompleteTasksHolder.appendChild(listItem);
    bindTaskEvents(listItem, taskCompleted);
}

// Edit and existing task
var editTask = function() {
    console.log("edit task");
    // When the edit button is pressed
        // if the class of the parent is .editMode
            // switch from editMode
            // label text become the input's value
        // else
            // switch to editMode
            // input value becomes the label's text
        // toggle .editMode on the parent
}

// Delete an existing task
var deleteTask = function() {
    console.log("delete task");
    // When the delete button is pressed
        // remove the parent list item from the ul
}

// Mark a task as complete
var taskCompleted = function() {
    console.log("task complete");
    // append the task list item to the completed-tasks
    var listItem = this.parentNode;
    completedTasksHolder.appendChild(listItem);
    bindTaskEvents(listItem, taskIncomplete);
}

// Mark a task as incomplete
var taskIncomplete = function() {
    console.log("task incomplete");
    // append to the incomplete-tasks
    var listItem = this.parentNode;
    incompleteTasksHolder.appendChild(listItem);
    bindTaskEvents(listItem, taskCompleted);
}

var bindTaskEvents = function(taskListItem, checkBoxEventHandler) {
    console.log("bind events");
    // select it's children
    var checkBox = taskListItem.querySelector("input[type=checkbox]");
    var editButton = taskListItem.querySelector("button.edit");
    var deleteButton = taskListItem.querySelector("button.delete");

    // bind editTask to edit button
    editButton.onclick = editTask;

    // bind deleteTask to the deleteButton
    deleteButton.onclick = deleteTask;

    // bind checkBoxEventHandler to checkbox
    checkBox.onchange = checkBoxEventHandler;

}

// Set the click handler to the addTask function
addButton.onclick = addTask;

// cycle over incompleteTaskHolder ul list items
for (var i = 0; i < incompleteTasksHolder.children.length; i++) {
    // bind events to list item's children (taskCompleted)
    bindTaskEvents(incompleteTasksHolder.children[i], taskCompleted);
}

//cycle over completeTaskHolder ul list items
for (var i = 0; i < completedTasksHolder.children.length; i++) {
    // bind events to list item's children (taskCompleted)
    bindTaskEvents(completedTasksHolder.children[i], taskIncomplete);
}